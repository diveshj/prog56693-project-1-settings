# SpaceShooter

Tool to change JSON data that [SpaceShooter](https://gitlab.com/diveshj/spaceshooter) uses to change sprites, damage values, window options and UI.

```
Open relevant JSON files SpaceShooter/Assets/JSON/ using the tool and then save as per requirement.
```
# Images
![Editor Data](https://gitlab.com/diveshj/project-images-dump/-/raw/main/SpaceShooter%20WPF%20Editor/Editor.jpg)
