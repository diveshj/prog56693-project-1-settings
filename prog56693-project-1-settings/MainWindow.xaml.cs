﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;

namespace prog56693_project_1_settings
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AsteroidController AsteroidCLocationObj;
        public string AsteroidControllerLoc = null;

        Player PlayerObj;
        public string PlayerLoc = null;

        Enemy EnemyObj;
        public string EnemyLoc = null;


        windowAttr windowObj;
        public string WindowLoc = null;

        UIController UIObj;
        public string UILoc = null;


        public MainWindow()
        {
            InitializeComponent();
            AsteroidCLocationObj = new AsteroidController();
            PlayerObj = new Player();
            EnemyObj = new Enemy();
            windowObj = new windowAttr();
            UIObj = new UIController();


        }

        #region Asteroids 
        //Asteroids Controller File Loads and Saves.
        private void AsteroidMeteorSmallOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                AsteroidCLocationObj.meteorSmall = dlg.FileName;
                AsteroidMeteorSmallTextBox.Text = AsteroidCLocationObj.meteorSmall;
            }
        }

        private void AsteroidMeteorBigOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                AsteroidCLocationObj.meteorBig = dlg.FileName;
                AsteroidMeteorBigTextBox.Text = AsteroidCLocationObj.meteorBig;
            }

        }


        private void AsteroidLoadDataJSON(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "JSON (*.json)|*.json";
            if (dlg.ShowDialog() == true)
            {
                AsteroidControllerLoc = dlg.FileName;
                string jsonstring = File.ReadAllText(AsteroidControllerLoc);
                AsteroidCLocationObj = JsonSerializer.Deserialize<AsteroidController>(jsonstring);
                AsteroidMeteorSmallTextBox.Text = AsteroidCLocationObj.meteorSmall;
                AsteroidMeteorBigTextBox.Text = AsteroidCLocationObj.meteorBig;

            }
        }


        private void AsteroidSaveDataJSON(object sender, RoutedEventArgs e)
        {
            if (AsteroidCLocationObj.meteorBig != null && AsteroidCLocationObj.meteorSmall != null)

            {
                if (AsteroidControllerLoc != null)
                {
                    JsonSerializerOptions options = new()
                    {
                        WriteIndented = true
                    };
                    string jsonstring = JsonSerializer.Serialize(AsteroidCLocationObj, options);
                    File.WriteAllText(AsteroidControllerLoc, jsonstring);
                    AsteroidErrorCode.Text = "Success!";
                    AsteroidErrorCode.Foreground = Brushes.Green;
                }

                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Title = "Select File To Save";
                    dlg.Filter = "JSON (*.json)|*.json";
                    if (dlg.ShowDialog() == true)
                    {
                        JsonSerializerOptions options = new()
                        {
                            WriteIndented = true
                        };
                        AsteroidControllerLoc = dlg.FileName;
                        string jsonstring = JsonSerializer.Serialize(AsteroidCLocationObj, options);
                        File.WriteAllText(AsteroidControllerLoc, jsonstring);
                        AsteroidErrorCode.Text = "Success!";
                        AsteroidErrorCode.Foreground = Brushes.Green;

                    }
                }
            }
            else
            {
                AsteroidErrorCode.Text = "One or more of the required data is not set!";
            }
        }
        #endregion


        #region Player
        //Player Data loading saving
        private void PlayerShipTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                PlayerObj.shipTexture= dlg.FileName;
                PlayershipTextureText.Text = PlayerObj.shipTexture;
            }
        }

        private void PlayerInitProjTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                PlayerObj.initProjTexture = dlg.FileName;
                PlayerinitProjTextureText.Text = PlayerObj.initProjTexture;
            }
        }

        private void PlayerProjTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                PlayerObj.projTexture = dlg.FileName;
                PlayerprojTexture.Text = PlayerObj.projTexture;
            }
        }

        private void PlayerHPValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);

        }

        private void PlayerDamageValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);

        }

        private void PlayerLoadDataJSON(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "JSON (*.json)|*.json";
            if (dlg.ShowDialog() == true)
            {
                PlayerLoc = dlg.FileName;
                string jsonstring = File.ReadAllText(PlayerLoc);
                PlayerObj = JsonSerializer.Deserialize<Player>(jsonstring);

                PlayershipTextureText.Text = PlayerObj.shipTexture;
                PlayerinitProjTextureText.Text = PlayerObj.initProjTexture;
                PlayerprojTexture.Text = PlayerObj.projTexture;
                PlayerHP.Text = Convert.ToString(PlayerObj.HP);
                PlayerDamage.Text = Convert.ToString(PlayerObj.damage);
            }
        }

        private void PlayerdSaveDataJSON(object sender, RoutedEventArgs e)
        {
            if (PlayerObj.shipTexture!= null && PlayerObj.initProjTexture!= null && PlayerObj.projTexture != null && Convert.ToInt32(PlayerDamage.Text) !=0  && Convert.ToInt32(PlayerHP.Text) !=0)

            {
                if (PlayerLoc != null)
                {
                    PlayerObj.damage = Convert.ToInt32(PlayerDamage.Text);
                    PlayerObj.HP = Convert.ToInt32(PlayerHP.Text);

                    JsonSerializerOptions options = new()
                    {
                        WriteIndented = true
                    };
                    string jsonstring = JsonSerializer.Serialize(PlayerObj, options);
                    File.WriteAllText(PlayerLoc, jsonstring);
                    PlayerErrorCode.Text = "Success!";
                    PlayerErrorCode.Foreground = Brushes.Green;
                }

                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Title = "Select File To Save";
                    dlg.Filter = "JSON (*.json)|*.json";
                    if (dlg.ShowDialog() == true)
                    {
                        PlayerObj.damage = Convert.ToInt32(PlayerDamage.Text);
                        PlayerObj.HP = Convert.ToInt32(PlayerHP.Text);

                        JsonSerializerOptions options = new()
                        {
                            WriteIndented = true
                        };
                        PlayerLoc = dlg.FileName;
                        string jsonstring = JsonSerializer.Serialize(PlayerObj, options);
                        File.WriteAllText(PlayerLoc, jsonstring);
                        PlayerErrorCode.Text = "Success!";
                        PlayerErrorCode.Foreground = Brushes.Green;

                    }
                }
            }
            else
            {
                PlayerErrorCode.Text = "One or more of the required data is not set!";
            }

        }
        #endregion


        #region Enemy
        //Enemy data loading and saving JSON
        private void EnemyShipTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                EnemyObj.shipTexture = dlg.FileName;
                EnemyshipTextureText.Text = EnemyObj.shipTexture;
            }

        }

        private void EnemyInitProjTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                EnemyObj.initProjTexture = dlg.FileName;
                EnemyinitProjTextureText.Text = EnemyObj.initProjTexture;
            }

        }

        private void EnemyProjTextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                EnemyObj.projTexture = dlg.FileName;
                EnemyProjTextureText.Text = EnemyObj.projTexture;
            }

        }

        private void EnemyHPValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void EnemyDamageValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void EnemyScoreValidaton(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void EnemyLoadDataJSON(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "JSON (*.json)|*.json";
            if (dlg.ShowDialog() == true)
            {
                EnemyLoc = dlg.FileName;
                string jsonstring = File.ReadAllText(EnemyLoc);
                EnemyObj = JsonSerializer.Deserialize<Enemy>(jsonstring);

                EnemyshipTextureText.Text = EnemyObj.shipTexture;
                EnemyinitProjTextureText.Text = EnemyObj.initProjTexture;
                EnemyProjTextureText.Text = EnemyObj.projTexture;
                EnemyHP.Text = Convert.ToString(EnemyObj.HP);
                EnemyDamage.Text = Convert.ToString(EnemyObj.damage);
                EnemyScoreVal.Text = Convert.ToString(EnemyObj.scoreVal);
            }
        }

        private void EnemySaveDataJSON(object sender, RoutedEventArgs e)
        {
            if (EnemyObj.shipTexture != null && EnemyObj.initProjTexture != null && EnemyObj.projTexture != null && Convert.ToInt32(EnemyDamage.Text) != 0 && Convert.ToInt32(EnemyHP.Text) != 0 && Convert.ToInt32(EnemyScoreVal.Text) != 0)

            {
                if (EnemyLoc != null)
                {
                    EnemyObj.damage = Convert.ToInt32(EnemyDamage.Text);
                    EnemyObj.HP = Convert.ToInt32(EnemyHP.Text);
                    EnemyObj.scoreVal = Convert.ToInt32(EnemyScoreVal.Text);

                    JsonSerializerOptions options = new()
                    {
                        WriteIndented = true
                    };
                    string jsonstring = JsonSerializer.Serialize(EnemyObj, options);
                    File.WriteAllText(EnemyLoc, jsonstring);
                    EnemyErrorCode.Text = "Success!";
                    EnemyErrorCode.Foreground = Brushes.Green;
                }

                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Title = "Select File To Save";
                    dlg.Filter = "JSON (*.json)|*.json";
                    if (dlg.ShowDialog() == true)
                    {
                        EnemyObj.damage = Convert.ToInt32(EnemyDamage.Text);
                        EnemyObj.HP = Convert.ToInt32(EnemyHP.Text);
                        EnemyObj.scoreVal = Convert.ToInt32(EnemyScoreVal.Text);

                        JsonSerializerOptions options = new()
                        {
                            WriteIndented = true
                        };
                        EnemyLoc = dlg.FileName;
                        string jsonstring = JsonSerializer.Serialize(EnemyObj, options);
                        File.WriteAllText(EnemyLoc, jsonstring);
                        EnemyErrorCode.Text = "Success!";
                        EnemyErrorCode.Foreground = Brushes.Green;

                    }
                }
            }
            else
            {
                EnemyErrorCode.Text = "One or more of the required data is not set!";
            }
        }



        #endregion

        #region Window
        //Window data JSON Load Save

        private void WindowHeightValidaton(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void WindowWidthValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");

            e.Handled = regex.IsMatch(e.Text);
        }
        private void WindowTitleValidaton(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-zA-Z0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void WindowLoadDataJSON(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "JSON (*.json)|*.json";
            if (dlg.ShowDialog() == true)
            {
                WindowLoc = dlg.FileName;
                string jsonstring = File.ReadAllText(WindowLoc);
                windowObj = JsonSerializer.Deserialize<windowAttr>(jsonstring);

                WindowWidthText.Text = Convert.ToString(windowObj.width);
                WindowHeightText.Text = Convert.ToString(windowObj.height);
                WindowTitleText.Text = windowObj.title;
            }
        }

        private void WindowSaveDataJSON(object sender, RoutedEventArgs e)
        {

            if (Convert.ToInt32(WindowWidthText.Text) != 0 && Convert.ToInt32(WindowHeightText.Text) != 0 && Convert.ToString(WindowTitleText.Text) != null)
            {
                if (WindowLoc != null)
                {
                    windowObj.height = Convert.ToInt32(WindowHeightText.Text);
                    windowObj.width = Convert.ToInt32(WindowWidthText.Text);
                    windowObj.title = WindowTitleText.Text;
                    JsonSerializerOptions options = new()
                    {
                        WriteIndented = true
                    };
                    string jsonstring = JsonSerializer.Serialize(windowObj, options);
                    File.WriteAllText(WindowLoc, jsonstring);
                    WindowErrorCode.Text = "Success!";
                    WindowErrorCode.Foreground = Brushes.Green;
                }
                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Title = "Select File To Save";
                    dlg.Filter = "JSON (*.json)|*.json";
                    if (dlg.ShowDialog() == true)
                    {
                        windowObj.height = Convert.ToInt32(WindowHeightText.Text);
                        windowObj.width = Convert.ToInt32(WindowWidthText.Text);
                        windowObj.title = WindowTitleText.Text;
                        JsonSerializerOptions options = new()
                        {
                            WriteIndented = true
                        };
                        WindowLoc = dlg.FileName;
                        string jsonstring = JsonSerializer.Serialize(windowObj, options);
                        File.WriteAllText(WindowLoc, jsonstring);
                        WindowErrorCode.Text = "Success!";
                        WindowErrorCode.Foreground = Brushes.Green;
                    }
                }
            }
            else
            {
                WindowErrorCode.Text = "One or more of the required data is not set!";
            }

        }

        #endregion

        #region UI
        private void UIHighScoreValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void UIFontOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "True Type Font File |*.ttf";
            if (dlg.ShowDialog() == true)
            {
                UIObj.font = dlg.FileName;
                UIFontText.Text = UIObj.font;
            }
        }
        private void UITextureOpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "Image File |*.jpg; *.bmp; *.jpeg; *.png";
            if (dlg.ShowDialog() == true)
            {
                UIObj.textLives = dlg.FileName;
                UILivesTextureText.Text = UIObj.textLives;
            }
        }

        private void UILoadDataJSON(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select File To Load";
            dlg.Filter = "JSON (*.json)|*.json";
            if (dlg.ShowDialog() == true)
            {
                UILoc = dlg.FileName;
                string jsonstring = File.ReadAllText(UILoc);
                UIObj = JsonSerializer.Deserialize<UIController>(jsonstring);

                UIFontText.Text = UIObj.font;
                UILivesTextureText.Text = UIObj.textLives;
                UIHighscore.Text = Convert.ToString(UIObj.Highscore);
            }
        }

        private void UISaveDataJSON(object sender, RoutedEventArgs e)
        {
            if (UIObj.font!= null && Convert.ToInt32(UIHighscore.Text) != 0 && UIObj.textLives != null)

            {
                if (UILoc != null)
                {
                    UIObj.Highscore= Convert.ToInt32(UIHighscore.Text);

                    JsonSerializerOptions options = new()
                    {
                        WriteIndented = true
                    };
                    string jsonstring = JsonSerializer.Serialize(UIObj, options);
                    File.WriteAllText(UILoc, jsonstring);
                    UIErrorCode.Text = "Success!";
                    UIErrorCode.Foreground = Brushes.Green;
                }

                else
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Title = "Select File To Save";
                    dlg.Filter = "JSON (*.json)|*.json";
                    if (dlg.ShowDialog() == true)
                    {
                        UIObj.Highscore = Convert.ToInt32(UIHighscore.Text);

                        JsonSerializerOptions options = new()
                        {
                            WriteIndented = true
                        };
                        UILoc = dlg.FileName;
                        string jsonstring = JsonSerializer.Serialize(UIObj, options);
                        File.WriteAllText(UILoc, jsonstring);
                        UIErrorCode.Text = "Success!";
                        UIErrorCode.Foreground = Brushes.Green;

                    }
                }
            }
            else
            {
                UIErrorCode.Text = "One or more of the required data is not set!";
            }

        }
        #endregion

    }
}
