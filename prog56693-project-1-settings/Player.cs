﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog56693_project_1_settings
{
    class Player
    {
        public int HP { get; set; }
        public string shipTexture { get; set; }
        public string initProjTexture { get; set; }
        public string projTexture { get; set; }
        public int damage { get; set; }

    }
}
